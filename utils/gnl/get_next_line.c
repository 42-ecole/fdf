/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/06 20:30:51 by rfunk             #+#    #+#             */
/*   Updated: 2019/02/18 19:30:15 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static char		*parse_line(char **arr, int fd)
{
	char	*temp;
	char	*line;
	int		i;

	i = 0;
	if (!(arr[fd]))
		return (0);
	temp = arr[fd];
	while (temp[i] != '\n' && temp[i])
		i++;
	line = NULL;
	line = ft_strsub(temp, 0, i);
	if (i < (int)ft_strlen(temp))
		arr[fd] = ft_strdup(&temp[i + 1]);
	else
		arr[fd] = ft_strdup("");
	free(temp);
	return (line);
}

int				get_next_line(const int fd, char **line)
{
	char		buf[BUFF_SIZE + 1];
	int			bytes;
	static char	**arr;

	if (!arr)
		arr = (char **)malloc(sizeof(char *) * STACK_SIZE);
	if (fd < 0 || !line || read(fd, buf, 0) < 0)
		return (-1);
	if (!arr[fd])
		arr[fd] = ft_strnew(0);
	if (!(ft_strint(arr[fd], '\n')))
		while ((bytes = read(fd, buf, BUFF_SIZE)))
		{
			buf[bytes] = 0;
			if (!(arr[fd] = ft_strjoinfree(arr[fd], buf)))
				return (-1);
			if (ft_strchr(buf, '\n'))
				break ;
		}
	if (ft_strlen(arr[fd]))
	{
		*line = parse_line(arr, fd);
		return (1);
	}
	return (0);
}
