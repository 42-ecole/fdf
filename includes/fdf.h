/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/08 20:52:14 by rfunk             #+#    #+#             */
/*   Updated: 2019/02/26 20:29:45 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include <mlx.h>
# include <math.h>
# include <stdlib.h>
# include <stdio.h>
# include "../utils/libft/libft.h"
# include "../utils/gnl/get_next_line.h"

# define UNIQ_BPP 4

typedef struct	s_matrix
{
	double			**grid;
	int				len;
	int				size;
	int				value;
	double			scale;
}				t_matrix;

typedef struct	s_point
{
	int			value;
	int			color;
}				t_point;

typedef	struct	s_line
{
	t_point			x0;
	t_point			x1;
	t_point			y0;
	t_point			y1;
}				t_line;

typedef	struct	s_img
{
	void			*image;
	char			*ptr;
	int				bpp;
	int				size_line;
	int				endian;
}				t_img;

typedef struct	s_mouse
{
	int				pressed;
	double			x;
	double			y;
	double			prev_x;
	double			prev_y;
}				t_mouse;

typedef struct	s_event
{
	void			*mlx_ptr;
	void			*win_ptr;
	int				width;
	int				height;
	int				min_height;
	int				*colors;
	int				base_color;
	int				pflag;
	t_matrix		shape;
	t_matrix		transform;
	t_matrix		height_map;
	t_img			img;
	t_mouse			mouse;
}				t_event;

int				get_next_line(const int fd, char **line);
int				read_map(t_event *event, const char **argv);
void			clear_image(t_img *img, t_event *event);
void			set_pixel(t_event *event, int x, int y, int color);
void			init_image(t_event *event);
int				mouse_press(int mouse_code, int x, int y, t_event *event);
int				mouse_release(int mouse_code, int x, int y, t_event *event);
int				mouse_move(int x, int y, t_event *event);
int				key_press(int key_code, t_event *event);
void			draw_line(t_line line, t_event *event);
void			draw_matrix(double **matrix, t_event *event);
void			set_line_fields(t_line *line,
				double **matrix, int j, int offset, t_event *event);
void			init_matrix(t_matrix *tr, int size, int len);
void			init_coords(t_event *event);
double			**apply(t_matrix tr_matrix, t_matrix matrix);
double			**apply2(t_matrix first, t_matrix second);
void			fill_tr(t_event *event);
void			rotate_y(t_event *event, double angle);
void			rotate_x(t_event *event, double angle);
void			rotate_z(t_event *event, double angle);
void			destroy_matrix(t_matrix *tr);
void			perspective(t_event *event);
int				parse_map_without_color(char **rows, t_event *event);
int				parse_map_with_color(char **rows,
				t_matrix *hmap, t_event *event);
void			get_lmap_len(char **rows, t_event *event);
int				is_16basechr(char c);
void			hate_norminette(t_event *event, int i, int j, char **fs);
int				count_cols(char **els);
void			set_ui(t_event *event, int color);
void			parallel(t_event *event);
void			iso(t_event *event);

#endif
