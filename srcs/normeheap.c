/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   normeheap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/26 17:52:23 by rfunk             #+#    #+#             */
/*   Updated: 2019/02/26 17:56:42 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fdf.h"

void	get_lmap_len(char **rows, t_event *event)
{
	int		i;
	int		space_trigger;

	i = 0;
	space_trigger = 0;
	while (rows[0][i])
	{
		if (rows[0][i] == ' ' && !space_trigger)
		{
			space_trigger = 1;
			event->height_map.len++;
		}
		else
			space_trigger = 0;
		i++;
	}
}

int		is_16basechr(char c)
{
	if (c != 'a' && c != 'b' && c != 'c'
		&& c != 'd' && c != 'e' && c != 'f'
		&& c != 'A' && c != 'B' && c != 'C'
		&& c != 'D' && c != 'E' && c != 'F')
		return (1);
	return (0);
}

void	hate_norminette(t_event *event, int i, int j, char **fs)
{
	event->height_map.grid[i][j] = (double)ft_atoi(fs[0]);
	if (fs[1])
		event->colors[i * event->height_map.len + j] = ft_atoi_base(fs[1], 16);
	else
		event->colors[i * event->height_map.len + j] = event->base_color;
	free(fs[0]);
	free(fs[1]);
}

int		count_cols(char **els)
{
	int	count;

	if (!els)
		return (0);
	count = 0;
	while (els[count])
		count++;
	return (count);
}
