/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/08 18:52:55 by rfunk             #+#    #+#             */
/*   Updated: 2019/02/26 20:03:55 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fdf.h"

void	global_destroy(t_event *event)
{
	destroy_matrix(&(event->shape));
	destroy_matrix(&(event->transform));
	destroy_matrix(&(event->height_map));
}

int		global_init(t_event *event, const char **argv)
{
	event->base_color = 0x339900;
	event->width = 1920;
	event->height = 1080;
	event->mlx_ptr = mlx_init();
	event->win_ptr = mlx_new_window(event->mlx_ptr,
	event->width, event->height, "FDF");
	event->pflag = 0;
	if (!(read_map(event, argv)))
		return (0);
	if (event->height_map.size > event->height_map.len)
		event->transform.scale = (event->height / event->height_map.size) - 5;
	else
		event->transform.scale = (event->width / event->height_map.len) - 5;
	init_matrix(&(event->transform), 4, 4);
	init_coords(event);
	init_image(event);
	return (1);
}

int		main(int argc, const char **argv)
{
	t_event event;

	ft_memset((char *)&event, 0, sizeof(t_event));
	if (argc != 2)
	{
		global_destroy(&event);
		return (-1);
	}
	if (!(global_init(&event, argv)))
	{
		global_destroy(&event);
		return (-1);
	}
	fill_tr(&event);
	iso(&event);
	mlx_hook(event.win_ptr, 4, 0, mouse_press, &event);
	mlx_hook(event.win_ptr, 5, 0, mouse_release, &event);
	mlx_hook(event.win_ptr, 6, 0, mouse_move, &event);
	mlx_hook(event.win_ptr, 2, 0, key_press, &event);
	mlx_loop(event.mlx_ptr);
	global_destroy(&event);
	return (0);
}
