/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   image.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/08 00:16:55 by rfunk             #+#    #+#             */
/*   Updated: 2019/02/26 20:07:29 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fdf.h"

void	init_image(t_event *event)
{
	event->img.image = mlx_new_image(event->mlx_ptr,
		event->width, event->height);
	event->img.ptr = mlx_get_data_addr(event->img.image, &event->img.bpp,
			&event->img.size_line, &event->img.endian);
	event->img.bpp /= 8;
}

void	set_pixel(t_event *event, int x, int y, int color)
{
	if (x < 0 || x >= event->width || y < 0 || y >= event->height)
		return ;
	((int*)(event->img.ptr))[y * event->width + x] = color;
}

void	set_ui(t_event *event, int color)
{
	mlx_string_put(event->mlx_ptr, event->win_ptr, 50,
	50, color, " Move the figure : W, A, S, D");
	mlx_string_put(event->mlx_ptr, event->win_ptr, 50,
	65, color, " Rotate Z - axis : Q, R");
	mlx_string_put(event->mlx_ptr, event->win_ptr, 50,
	80, color, "   X, Y - axes   : Drag mouse ");
	mlx_string_put(event->mlx_ptr, event->win_ptr, 50,
	95, color, "   Projections   : 1, 2, 3");
	mlx_string_put(event->mlx_ptr, event->win_ptr, 50,
	110, color, "     Scale       : Scroll up/down");
}

void	clear_image(t_img *img, t_event *event)
{
	ft_bzero(img->ptr, event->width * event->height * img->bpp);
}
