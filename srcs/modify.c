/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   modify.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/26 17:26:39 by rfunk             #+#    #+#             */
/*   Updated: 2019/02/26 20:04:15 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fdf.h"

void	perspective(t_event *event)
{
	t_matrix tr;

	init_matrix(&tr, 4, 4);
	tr.grid[0][0] = 1;
	tr.grid[1][1] = 1;
	tr.grid[2][2] = 1;
	tr.grid[3][3] = 1;
	tr.grid[3][0] = -0.2;
	event->transform.grid = apply(event->transform, tr);
	destroy_matrix(&tr);
}

void	parallel(t_event *event)
{
	fill_tr(event);
	draw_matrix(apply(event->transform, event->shape), event);
}

void	iso(t_event *event)
{
	rotate_y(event, -0.523599);
	rotate_x(event, -0.523599);
	rotate_z(event, 0.785398);
	draw_matrix(apply(event->transform, event->shape), event);
}
