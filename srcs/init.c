/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/08 00:48:47 by rfunk             #+#    #+#             */
/*   Updated: 2019/02/26 15:19:47 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fdf.h"

void	destroy_matrix(t_matrix *tr)
{
	int i;

	i = 0;
	if (!tr->grid)
		return ;
	while (i < tr->size)
	{
		free(tr->grid[i]);
		i++;
	}
	free(tr->grid);
	tr->grid = NULL;
}

void	init_matrix(t_matrix *tr, int size, int len)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	tr->grid = (double **)malloc(sizeof(double *) * size);
	while (i < size)
	{
		j = 0;
		tr->grid[i] = (double *)malloc(sizeof(double) * len);
		while (j < len)
		{
			tr->grid[i][j] = 0;
			j++;
		}
		i++;
	}
	tr->size = size;
	tr->len = len;
}

void	init_coords(t_event *event)
{
	int x;
	int y;
	int i;

	i = 0;
	x = 0;
	y = 0;
	init_matrix(&(event->shape), 4, event->height_map.size
	* event->height_map.len);
	while (y < event->height_map.size)
	{
		x = 0;
		while (x < event->height_map.len)
		{
			event->shape.grid[0][i] = x - event->height_map.len / 2;
			event->shape.grid[1][i] = y - event->height_map.size / 2;
			event->shape.grid[2][i] = (event->height_map.grid[y][x]);
			event->shape.grid[3][i] = 1;
			i++;
			x++;
		}
		y++;
	}
}
