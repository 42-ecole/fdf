/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_map.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/18 20:06:47 by rfunk             #+#    #+#             */
/*   Updated: 2019/02/26 17:55:01 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fdf.h"

int			parse_map_with_color(char **rows, t_matrix *hmap, t_event *event)
{
	int		i;
	int		j;
	int		count;
	char	**els;
	char	**fs;

	i = 0;
	event->colors = (int *)malloc(sizeof(int) * hmap->size * hmap->len);
	while (i < hmap->size)
	{
		j = 0;
		els = ft_strsplit(rows[i], ' ');
		(i == 0) ? count = count_cols(els) : 0;
		while (els[j])
		{
			fs = ft_strsplit(els[j], ',');
			hate_norminette(event, i, j, fs);
			ft_strdel(&els[j]);
			j++;
		}
		if (count != j)
			return (0);
		i++;
	}
	return (1);
}

int			parse_map_without_color(char **rows, t_event *event)
{
	int		i;
	int		j;
	int		count;
	char	**els;

	i = 0;
	count = 0;
	while (i < event->height_map.size)
	{
		j = 0;
		els = ft_strsplit(rows[i], ' ');
		(i == 0) ? count = count_cols(els) : 0;
		while (els[j])
		{
			event->height_map.grid[i][j] = (double)ft_atoi(els[j]);
			ft_strdel(&els[j]);
			free(els[j]);
			j++;
		}
		free(els);
		if (count != j)
			return (0);
		i++;
	}
	return (1);
}

int			check_valid(char **rows, int size, int color)
{
	int		i;
	int		j;

	i = 0;
	while (i < size)
	{
		j = 0;
		while (rows[i][j])
		{
			if (!ft_isdigit(rows[i][j]) && rows[i][j] != ' '
			&& rows[i][j] != 'x' && rows[i][j] != 'X' && rows[i][j] != ','
			&& rows[i][j] != '-' && rows[i][j] != '+')
			{
				if (color && is_16basechr(rows[i][j]))
					return (0);
				else if (!color)
					return (0);
			}
			j++;
		}
		i++;
	}
	return (1);
}

int			parse_map(char **rows, t_event *event, int size)
{
	int		color;
	int		i;

	if (!rows[0])
		return (0);
	i = -1;
	color = 0;
	while (++i < size)
		if (ft_strint(rows[i], ','))
			color = 1;
	if (!check_valid(rows, size, color))
		return (0);
	get_lmap_len(rows, event);
	init_matrix(&(event->height_map), size, event->height_map.len + 1);
	if (!color)
	{
		if (!parse_map_without_color(rows, event))
			return (0);
	}
	else
	{
		if (!parse_map_with_color(rows, &(event->height_map), event))
			return (0);
	}
	return (1);
}

int			read_map(t_event *event, const char **argv)
{
	int		fd;
	char	**rows;
	char	*tmp;
	int		size;
	int		i;

	size = 0;
	i = 0;
	tmp = ft_strnew(0);
	fd = open(argv[1], O_RDONLY);
	while (get_next_line(fd, &tmp) > 0)
		size++;
	close(fd);
	if (size == 0)
		return (0);
	fd = open(argv[1], O_RDONLY);
	rows = (char **)malloc(sizeof(char *) * size);
	while (get_next_line(fd, &rows[i]) > 0)
		i++;
	if (!parse_map(rows, event, size))
		return (0);
	return (1);
}
