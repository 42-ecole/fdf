/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hook.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/08 00:22:14 by rfunk             #+#    #+#             */
/*   Updated: 2019/02/26 20:27:05 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fdf.h"

int			mouse_press(int mouse_code, int x, int y, t_event *event)
{
	clear_image(&event->img, event);
	if (mouse_code == 1)
	{
		if (event->mouse.pressed == 0)
		{
			event->mouse.pressed = 1;
			event->mouse.x = x;
			event->mouse.y = y;
		}
		else
			event->mouse.pressed = 0;
	}
	if (mouse_code == 4)
		event->transform.grid[3][3] *= 1.1;
	else if (mouse_code == 5)
		event->transform.grid[3][3] /= 1.1;
	draw_matrix(apply(event->transform, event->shape), event);
	return (0);
}

int			mouse_release(int mouse_code, int x, int y, t_event *event)
{
	(void)mouse_code;
	(void)x;
	(void)y;
	event->mouse.pressed = 0;
	return (0);
}

int			mouse_move(int x, int y, t_event *event)
{
	clear_image(&event->img, event);
	if (x >= 0 && x <= event->width && y >= 0 && y <= event->height)
	{
		event->mouse.prev_x = event->mouse.x;
		event->mouse.prev_y = event->mouse.y;
		event->mouse.x = x;
		event->mouse.y = y;
		if (event->mouse.pressed == 1)
		{
			rotate_y(event, -(event->mouse.prev_y - y) / 1000);
			rotate_x(event, (event->mouse.prev_x - x) / 1000);
			draw_matrix(apply(event->transform, event->shape), event);
		}
	}
	return (0);
}

static void	key20_press(t_event *event)
{
	if (!event->pflag)
	{
		event->transform.grid[3][2] = -0.01 * event->transform.grid[2][2];
		event->pflag = 1;
	}
	else
	{
		event->transform.grid[3][2] = -0.01 * event->transform.grid[2][2];
		event->pflag = 0;
	}
}

int			key_press(int key_code, t_event *event)
{
	event->transform.grid[3][0] = 0;
	ft_putnbr(key_code);
	clear_image(&event->img, event);
	if (key_code == 53)
	{
		mlx_destroy_window(event->mlx_ptr, event->win_ptr);
		exit(1);
	}
	else if (key_code == 13)
		event->transform.grid[1][3] -= event->transform.scale;
	else if (key_code == 1)
		event->transform.grid[1][3] += event->transform.scale;
	else if (key_code == 0)
		event->transform.grid[0][3] -= event->transform.scale;
	else if (key_code == 2)
		event->transform.grid[0][3] += event->transform.scale;
	(key_code == 12) ? rotate_z(event, 0.174533) : 0;
	(key_code == 14) ? rotate_z(event, -0.174533) : 0;
	(key_code == 18) ? parallel(event) : 0;
	(key_code == 19) ? iso(event) : 0;
	(key_code == 20) ? key20_press(event) : 0;
	draw_matrix(apply(event->transform, event->shape), event);
	return (0);
}
