/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/08 19:45:35 by rfunk             #+#    #+#             */
/*   Updated: 2019/02/26 19:49:55 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fdf.h"

void	set_pixels(t_line line, int steep, t_event *event)
{
	int error;

	error = 0;
	while (line.x0.value <= line.x1.value)
	{
		if (steep)
		{
			set_pixel(event, line.y0.value + event->width / 2,
			line.x0.value + event->height / 2, line.x0.color);
		}
		else
			set_pixel(event, line.x0.value + event->width / 2,
			line.y0.value + event->height / 2, line.x0.color);
		error += abs(line.y1.value - line.y0.value) * 2;
		if (error > line.x1.value - line.x0.value)
		{
			line.y0.value += (line.y1.value > line.y0.value ? 1 : -1);
			error -= (line.x1.value - line.x0.value) * 2;
		}
		line.x0.value++;
	}
}

void	draw_line(t_line line, t_event *event)
{
	int steep;

	steep = 0;
	if (abs(line.x0.value - line.x1.value) < abs(line.y0.value - line.y1.value))
	{
		ft_swap(&(line.x0.value), &(line.y0.value));
		ft_swap(&(line.x1.value), &(line.y1.value));
		steep = 1;
	}
	if (line.x0.value > line.x1.value)
	{
		ft_swap(&(line.x0.value), &(line.x1.value));
		ft_swap(&(line.y0.value), &(line.y1.value));
	}
	set_pixels(line, steep, event);
}

void	set_line_fields1(t_line *line, double **matrix, int j, t_event *event)
{
	line->x0.value = round(matrix[0][j] / matrix[3][j]);
	line->y0.value = round(matrix[1][j] / matrix[3][j]);
	line->x1.value = round(matrix[0][j + 1] / matrix[3][j + 1]);
	line->y1.value = round(matrix[1][j + 1] / matrix[3][j + 1]);
	if (event->colors)
		line->x0.color = event->colors[j];
	else
		line->x0.color = event->base_color;
}

void	set_line_fields2(t_line *line, double **matrix, int j, t_event *event)
{
	line->x0.value = round(matrix[0][j] / matrix[3][j]);
	line->y0.value = round(matrix[1][j] / matrix[3][j]);
	line->x1.value = round(matrix[0][j + event->height_map.len]
	/ matrix[3][j + event->height_map.len]);
	line->y1.value = round(matrix[1][j + event->height_map.len]
	/ matrix[3][j + event->height_map.len]);
	if (event->colors)
		line->x0.color = event->colors[j];
	else
		line->x0.color = event->base_color;
}

void	draw_matrix(double **matrix, t_event *event)
{
	int		j;
	int		count;
	int		length;
	t_line	line;

	j = -1;
	count = 0;
	length = event->height_map.len * event->height_map.size;
	while (++j + 1 <= length)
	{
		if (count++ < event->height_map.len - 1)
			set_line_fields1(&line, matrix, j, event);
		else
			count = 0;
		draw_line(line, event);
		if (j + event->height_map.len < length)
			set_line_fields2(&line, matrix, j, event);
		draw_line(line, event);
	}
	mlx_put_image_to_window(event->mlx_ptr,
	event->win_ptr, event->img.image, 0, 0);
	set_ui(event, 0xFFFFFF);
}
