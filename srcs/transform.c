/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   transform.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/08 18:37:27 by rfunk             #+#    #+#             */
/*   Updated: 2019/02/26 17:26:28 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fdf.h"

double		**apply(t_matrix tr_matrix, t_matrix matrix)
{
	t_matrix	ret;
	int			i;
	int			j;
	int			k;

	i = 0;
	init_matrix(&ret, 4, matrix.len);
	while (i < 4)
	{
		j = 0;
		while (j < matrix.len)
		{
			ret.grid[i][j] = 0;
			k = 0;
			while (k < 4)
			{
				ret.grid[i][j] += (tr_matrix.grid[i][k] * matrix.grid[k][j]);
				k++;
			}
			j++;
		}
		i++;
	}
	return (ret.grid);
}

void		fill_tr(t_event *event)
{
	t_matrix tr;

	init_matrix(&tr, 4, 4);
	tr.grid[0][0] = event->transform.scale;
	tr.grid[1][1] = event->transform.scale;
	tr.grid[2][2] = event->transform.scale;
	tr.grid[3][3] = 1;
	event->transform.size = tr.size;
	event->transform.len = tr.len;
	event->transform.grid = tr.grid;
}

void		rotate_y(t_event *event, double angle)
{
	t_matrix tr;

	init_matrix(&tr, 4, 4);
	tr.grid[0][0] = 1;
	tr.grid[1][1] = cos(angle);
	tr.grid[1][2] = sin(angle);
	tr.grid[2][1] = -sin(angle);
	tr.grid[2][2] = cos(angle);
	tr.grid[3][3] = 1;
	event->transform.grid[3][2] = 0;
	event->transform.grid = apply(tr, event->transform);
	destroy_matrix(&tr);
}

void		rotate_x(t_event *event, double angle)
{
	t_matrix tr;

	init_matrix(&tr, 4, 4);
	tr.grid[0][0] = cos(angle);
	tr.grid[0][2] = -sin(angle);
	tr.grid[1][1] = 1;
	tr.grid[2][0] = sin(angle);
	tr.grid[2][2] = cos(angle);
	tr.grid[3][3] = 1;
	event->transform.grid[3][2] = 0;
	event->transform.grid = apply(tr, event->transform);
	destroy_matrix(&tr);
}

void		rotate_z(t_event *event, double angle)
{
	t_matrix tr;

	init_matrix(&tr, 4, 4);
	tr.grid[0][0] = cos(angle);
	tr.grid[0][1] = sin(angle);
	tr.grid[1][0] = -sin(angle);
	tr.grid[1][1] = cos(angle);
	tr.grid[2][2] = 1;
	tr.grid[3][3] = 1;
	event->transform.grid[3][2] = 0;
	event->transform.grid = apply(tr, event->transform);
	destroy_matrix(&tr);
}
