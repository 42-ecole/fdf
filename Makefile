NAME = fdf
FLAGS = -Wall -Wextra -Werror
INCLUDES = -I ./utils/libft/includes -I ./include -I ./utils/minilibx_macos -I ./utils/gnl
MLX = -L./utils/minilibx_macos -lmlx -framework OpenGL -framework AppKit
LIB = -L./utils/libft/ -lft
SRCS_DIR = $(addprefix ./srcs/, $(SRCS))
SRCS = main.c \
		read_map.c \
		image.c \
		hook.c \
		init.c \
		draw.c \
		transform.c \
		modify.c \
		normeheap.c
GNL_DIR = $(addprefix ./utils/gnl/, $(GNL))
GNL = get_next_line.c
ALLSRCS = $(SRCS) $(GNL)
OBJ_DIR = objs
OBJS = $(addprefix $(OBJ_DIR)/,$(ALLSRCS:.c=.o))

all: $(NAME)

$(NAME):
		make -C ./utils/libft
		make -C ./utils/minilibx_macos
		gcc -c $(FLAGS) $(SRCS_DIR) $(GNL_DIR) $(INCLUDES) -g
		mkdir -p $(OBJ_DIR) && mv $(SRCS:.c=.o) ./$(OBJ_DIR)/ && mv $(GNL:.c=.o) ./$(OBJ_DIR)/
		gcc $(GCCFLAGS) -o $(NAME) $(OBJS) $(LIB) $(MLX) $(INC)
clean:
		make clean -C ./utils/libft
		make clean -C ./utils/minilibx_macos
		rm -rf $(OBJ_DIR)

fclean: clean
		make fclean -C ./utils/libft
		make clean -C ./utils/minilibx_macos
		rm -rf $(NAME)
re: fclean all