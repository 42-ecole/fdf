#include "../includes/fdf.h"

void	print_matrix2(t_matrix *matrix)
{
	int i;
	int j;
	char *str;

	i = 0;
	while (i <	matrix->size)
	{
		j = 0;
		str = NULL;
		str = ft_strnew(0);
		while (j < matrix->len)
		{
			str = ft_strjoin(str,ft_itoa(matrix->grid[i][j]));
			str = ft_strjoin(str," ");
			j++;
		}
		ft_putendl(str);
		i++;
	}
}
void	print_matrix(t_matrix	*matrix)
{
	int j;
	
	j = 0;
	printf("x y z\n");
	while (j < matrix->len)
	{
		printf("%f %f %f %f\n",matrix->grid[0][j], matrix->grid[1][j],matrix->grid[2][j], matrix->grid[3][j]);
		j++;
	}
}

int	get_min_z(t_event *event)
{
	int i;
	int length;
	int min;

	i = 0;
	min = INT16_MAX;
	length = event->height_map.len * event->height_map.size;
	while (i < length)
	{
		if (event->shape.grid[2][i] < min)
			min = event->shape.grid[2][i];
		i++;
	}
	return (min);
}

int	get_max_z(t_event *event)
{
	int i;
	int length;
	int max;

	i = 0;
	max = 0;
	length = event->height_map.len * event->height_map.size;
	while (i < length)
	{
		if (event->shape.grid[2][i] > max)
			max = event->shape.grid[2][i];
		i++;
	}
	return (max);
}